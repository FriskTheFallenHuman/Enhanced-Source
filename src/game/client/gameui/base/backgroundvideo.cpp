#include "cbase.h"
#include "backgroundvideo.h"
#include <vgui/ISurface.h>
#include "vgui_hudvideo.h"
#include "VGUIMatSurface/IMatSystemSurface.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

using namespace vgui;

CBackgroundMovie *g_pBackgroundMovie = NULL;

CBackgroundMovie* BackgroundMovie()
{
	if ( !g_pBackgroundMovie )
		g_pBackgroundMovie = new CBackgroundMovie();

	return g_pBackgroundMovie;
}

CBackgroundMovie::CBackgroundMovie()
{
	m_nBIKMaterial = BIKMATERIAL_INVALID;
	m_nTextureID = -1;
	m_szCurrentMovie[0] = 0;
	m_nLastGameState = -1;
}

CBackgroundMovie::~CBackgroundMovie()
{
}

void CBackgroundMovie::SetCurrentMovie( const char *szFilename )
{
	if ( Q_strcmp( m_szCurrentMovie, szFilename ) )
	{
		if ( m_nBIKMaterial != BIKMATERIAL_INVALID )
		{
			// FIXME: Make sure the m_pMaterial is actually destroyed at this point!
			g_pBIK->DestroyMaterial( m_nBIKMaterial );
			m_nBIKMaterial = BIKMATERIAL_INVALID;
			m_nTextureID = -1;
		}

		char szMaterialName[ MAX_PATH ];
		Q_snprintf( szMaterialName, sizeof( szMaterialName ), "BackgroundBIKMaterial%i", g_pBIK->GetGlobalMaterialAllocationNumber() );
		m_nBIKMaterial = bik->CreateMaterial( szMaterialName, szFilename, "GAME", BIK_LOOP );

		Q_snprintf( m_szCurrentMovie, sizeof( m_szCurrentMovie ), "%s", szFilename );
	}
}

void CBackgroundMovie::ClearCurrentMovie()
{
	if ( m_nBIKMaterial != BIKMATERIAL_INVALID )
	{
		// FIXME: Make sure the m_pMaterial is actually destroyed at this point!
		g_pBIK->DestroyMaterial( m_nBIKMaterial );
		m_nBIKMaterial = BIKMATERIAL_INVALID;
		m_nTextureID = -1;
	}
}

int CBackgroundMovie::SetTextureMaterial()
{
	if ( m_nBIKMaterial == BIKMATERIAL_INVALID )
		return -1;

	if ( m_nTextureID == -1 )
		m_nTextureID = g_pMatSystemSurface->CreateNewTextureID( true );
	
	g_pMatSystemSurface->DrawSetTextureMaterial( m_nTextureID, g_pBIK->GetMaterial( m_nBIKMaterial ) );

	return m_nTextureID;
}

void CBackgroundMovie::Update()
{
	int nGameState = 0;
	if( nGameState != m_nLastGameState ) 
	{
		const char *pFilename = NULL;

		int nChosenMovie = RandomInt( 0, 4 );
		switch( nChosenMovie ) 
		{
			case 0: 
				pFilename = "media/background01.bik"; 
				break;
			default:
			case 1: 
				pFilename = "media/background02.bik"; 
				break;
			case 2: 
				pFilename = "media/background03.bik"; 
				break;
			case 3: 
				pFilename = "media/background04.bik";
				break;
			case 4: 
				pFilename = "media/background05.bik"; 
				break;
		}

		if( pFilename )
			SetCurrentMovie( pFilename );
	}

	m_nLastGameState = nGameState;

	if ( m_nBIKMaterial == BIKMATERIAL_INVALID )
		return;

	if ( g_pBIK->ReadyForSwap( m_nBIKMaterial ) )
	{
		if ( g_pBIK->Update( m_nBIKMaterial ) == false )
		{
			// FIXME: Make sure the m_pMaterial is actually destroyed at this point!
			g_pBIK->DestroyMaterial( m_nBIKMaterial );
			m_nBIKMaterial = BIKMATERIAL_INVALID;
		}
	}
}