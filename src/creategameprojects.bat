setlocal enabledelayedexpansion

@echo off
cls

rem Params goes right after the +game
set params=

rem Special parameter for compiler choice
set compiler=

echo Sub-Game Features
echo --------
echo.

set /p Episodic=Define Episodic Code? (y):
If not "!Episodic!"=="n" (
    set params=!params! /define:USE_EPISODIC_CODE
)

set /p SSE=Use Shader Editor? (y):
If not "!SSE!"=="n" (
    set params=!params! /define:USE_SHADEREDITOR
)

set /p Deferred=Use Deferred Lighting? (y):
If not "!Deferred!"=="n" (
    set params=!params! /define:USE_DEFERRED
)

echo.
echo Compiler Choices
echo --------
echo.

set /p VStudioVersion=Visual Studio [2010,2012,2013,2019]:

If "!VStudioVersion!"=="2010" (
    set compiler=!compiler! /2010
)

If "!VStudioVersion!"=="2012" (
    set compiler=!compiler! /2012
)

If "!VStudioVersion!"=="2013" (
    set compiler=!compiler! /2013
)

If "!VStudioVersion!"=="2019" (
    set compiler=!compiler! /2013 /define:VS2019
)

cls

title !params! !compiler!

devtools\bin\vpc.exe /hlx +game !params! /mksln games.sln !compiler!
@pause